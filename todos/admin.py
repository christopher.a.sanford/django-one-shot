from django.contrib import admin
from .models import TodoList, TodoItem

admin.site.register(TodoList)
admin.site.register(TodoItem)

class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
# Register your models here.
